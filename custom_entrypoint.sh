#!/bin/sh

version_greater() {
    [ "$(printf '%s\n' "$@" | sort -t '.' -n -k1,1 -k2,2 -k3,3 -k4,4 | head -n 1)" != "$1" ]
}

echo "Custom eCloud entrypoint"
rsync_options="-rlDog --chown www-data:www-data --delete"

SRC_DIR="/usr/src/nextcloud"
DST_DIR="/var/www/html"

installed_version="0.0.0.0"

if [ -f /var/www/html/version.php ]; then
    installed_version="$(php -r 'require "/var/www/html/version.php"; echo implode(".", $OC_Version);')"
fi
image_version="$(php -r 'require "/usr/src/nextcloud/version.php"; echo implode(".", $OC_Version);')"

if version_greater "$image_version" "$installed_version"; then
    rsync $rsync_options --include "/notes/" --exclude '/*' $SRC_DIR/custom_apps/ $DST_DIR/custom_apps/
    rsync $rsync_options --include "/calendar/" --exclude '/*' $SRC_DIR/custom_apps/ $DST_DIR/custom_apps/
    rsync $rsync_options --include "/contacts/" --exclude '/*' $SRC_DIR/custom_apps/ $DST_DIR/custom_apps/
    rsync $rsync_options --include "/user_backend_sql_raw/" --exclude '/*' $SRC_DIR/custom_apps/ $DST_DIR/custom_apps/
    rsync $rsync_options --include "/email-recovery/" --exclude '/*' $SRC_DIR/custom_apps/ $DST_DIR/custom_apps/
    rsync $rsync_options --include "/ecloud-accounts/" --exclude '/*' $SRC_DIR/custom_apps/ $DST_DIR/custom_apps/
    rsync $rsync_options --include "/ecloud-theme-helper/" --exclude '/*' $SRC_DIR/custom_apps/ $DST_DIR/custom_apps/
    rsync $rsync_options --include "/murena_launcher/" --exclude '/*' $SRC_DIR/custom_apps/ $DST_DIR/custom_apps/
    rsync $rsync_options --include "/integration_google/" --exclude '/*' $SRC_DIR/custom_apps/ $DST_DIR/custom_apps/
    rsync $rsync_options --include "/ldap_write_support/" --exclude '/*' $SRC_DIR/custom_apps/ $DST_DIR/custom_apps/
    rsync $rsync_options --include "/murena-dashboard/" --exclude '/*' $SRC_DIR/custom_apps/ /$DST_DIR/custom_apps/
    rsync $rsync_options --include "/snappymail/" --exclude '/*' $SRC_DIR/custom_apps/ /$DST_DIR/custom_apps/
    rsync $rsync_options --include "/eCloud/" --exclude '/*' $SRC_DIR/themes/ $DST_DIR/themes/
    rsync $rsync_options --include "/Murena/" --exclude '/*' $SRC_DIR/themes/ $DST_DIR/themes/
    rsync $rsync_options --include "/memories/" --exclude '/*' $SRC_DIR/custom_apps/ $DST_DIR/custom_apps/
    rsync $rsync_options --include "/drop_account/" --exclude '/*' $SRC_DIR/custom_apps/ $DST_DIR/custom_apps/
    rsync $rsync_options --include "/selfhost-theme-helper/" --exclude '/*' $SRC_DIR/custom_apps/ $DST_DIR/custom_apps/
else
    echo "Skipping rsync step as version not updated!"
fi

if [ "$(id -u)" = 0 ]; then
    su -p www-data -s /bin/sh -c "php $DST_DIR/occ config:system:set profile.enabled --value=false --type=boolean"
else
    sh -c "php $DST_DIR/occ config:system:set profile.enabled --value=false --type=boolean"
fi

/entrypoint.sh "$@"
