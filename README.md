# Custom nextcloud image for eCloud

This project builds a custom docker image from the official Nextcloud one, applying workarounds or specific behaviour of interest only for a shared, private-by-default installation such as ecloud.global.

## Building

Simply build as a standard docker image. Check `gitlab-ci.ym` for the commands we use.

## Using

We suggest you use our [ecloud-selfhosting](https://gitlab.e.foundation/e/infra/ecloud-selfhosting) project instead of this one directly. But if you wish to do so, then check our [releases page](https://gitlab.e.foundation/e/infra/ecloud/nextcloud/-/releases) and pull the latest tag from the container registry.

### To run ecloud locally(Tested on Ubuntu and Manjaro, should work on most linux distributions)

- Install [docker](https://docs.docker.com/engine/install/ubuntu/)(link is for Ubuntu)
- Install [docker-compose](https://docs.docker.com/compose/install/)
- Create a copy of the `ecloud_dev_example` directory locally where you want to install an `ecloud` development environment
- Use `cd` or file manager to enter the above directory
- Add a `.env` file with chosen attributes(example [.env](./ecloud_dev_example/.dev.env) file here, you can rename to `.env` to use same defaults)
- Set correct permissions to volumes:
  - `chown -R '33':'33' volumes/nextcloud/{html,data,log}`
- Pull the images and up the containers
  - `docker-compose pull`
  - `docker-compose up -d`

### Things to do on first installation locally

- Set config values, disable integrity check and refresh theme cache:

  - `docker exec -u www-data ecloud /var/www/html/occ config:system:set theme --value='eCloud'`
  - `docker exec -u www-data ecloud /var/www/html/occ config:system:set logfile --value='/var/www/log/nextcloud.log'`
  - `docker exec -u www-data ecloud /var/www/html/occ config:system:set loglevel --value='2' --type=integer`
  - `docker exec -u www-data ecloud /var/www/html/occ config:system:set integrity.check.disabled --value='true' --type=boolean`
  - `docker exec -u www-data ecloud /var/www/html/occ maintenance:theme:update`

- Disable apps:

  - `docker exec -u www-data ecloud /var/www/html/occ app:disable firstrunwizard`
  - `docker exec -u www-data ecloud /var/www/html/occ app:disable theming`
  - `docker exec -u www-data ecloud /var/www/html/occ app:disable files_external`

- Enable\Install apps:

  - `docker exec -u www-data ecloud /var/www/html/occ app:enable murena_launcher`
  - `docker exec -u www-data ecloud /var/www/html/occ app:enable ecloud-theme-helper`
  - `docker exec -u www-data ecloud /var/www/html/occ app:enable notes`
  - `docker exec -u www-data ecloud /var/www/html/occ app:enable news`
  - `docker exec -u www-data ecloud /var/www/html/occ app:enable quota_warning`
  - `docker exec -u www-data ecloud /var/www/html/occ app:enable contacts`
  - `docker exec -u www-data ecloud /var/www/html/occ app:enable calendar`
  - `docker exec -u www-data ecloud /var/www/html/occ app:enable email-recovery`
  - `docker exec -u www-data ecloud /var/www/html/occ app:enable ecloud-accounts`
  - `docker exec -u www-data ecloud /var/www/html/occ app:enable integration_google`
  - To install more apps, use `docker exec -u www-data ecloud /var/www/html/occ app:install $app` where `$app` is the name of the app

- To make the `html` folder editable to current user(`$USER`)(run commands with `sudo` if required):
  - `usermod -a -G '33' $USER`
  - `chmod -R g+w volumes/nextcloud/html`
  - Log out and log back into your system

## Contributing

Anyone can fork a project on our GitLab instance, but to prevent abuse it's disabled by default. Get in touch with us [by e-mail](mailto:dev@murena.com) or through our support channels and we will let you create a fork and submit MRs.
