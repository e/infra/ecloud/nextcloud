FROM nextcloud:28.0.9-fpm AS nextcloud
ARG BASE_DIR="/usr/src/nextcloud"
ARG TMP_PATCH_DIR="/tmp/build_patches"
ARG THEME_HELPER_JOB_ID="1004002"
ARG NOTES_VERSION="4.10.1"
ARG CONTACTS_JOB_ID="881946"
ARG CALENDAR_JOB_ID="991372"
ARG EMAIL_RECOVERY_JOB_ID="1004020"
ARG EA_JOB_ID="1004029"
ARG LAUNCHER_JOB_ID="991418"
ARG GOOGLE_INTEGRATION_VERSION="2.2.0"
ARG DASHBOARD_JOB_ID="1004045"
ARG SNAPPY_VERSION="2.37.2"
ARG SNAPPY_THEME_VERSION="4.0.1"
ARG USER_MIGRATION_JOB_ID="991454"
ARG MEMORIES_VERSION="7.3.1"
ARG DROP_ACCOUNT_VERSION="2.5.0"

RUN sed -i 's/28,0,9,1/28,0,9,6/' ${BASE_DIR}/version.php
COPY custom_entrypoint.sh /
RUN chmod +x /custom_entrypoint.sh
RUN mkdir -p /var/www/skeleton/Documents && mkdir -p /var/www/skeleton/Images
# Install unzip for unzipping artifacts
RUN apt-get update && apt-get install -y unzip ffmpeg

RUN curl -fsSL -o notes.tar.gz \
  "https://github.com/nextcloud-releases/notes/releases/download/v${NOTES_VERSION}/notes-v${NOTES_VERSION}.tar.gz" && \
  tar -xf notes.tar.gz -C ${BASE_DIR}/custom_apps/ && \
  rm notes.tar.gz;

# custom Contact
RUN curl -fsSL -o contacts.zip \
  "https://gitlab.e.foundation/e/infra/ecloud/nextcloud-apps/contacts/-/jobs/${CONTACTS_JOB_ID}/artifacts/download" && \
  unzip contacts.zip && \
  mv dist/contacts ${BASE_DIR}/custom_apps/ && \
  rm contacts.zip;

RUN curl -fsSL -o calendar.zip \
  "https://gitlab.e.foundation/e/infra/ecloud/nextcloud-apps/calendar/-/jobs/${CALENDAR_JOB_ID}/artifacts/download" && \
  unzip calendar.zip && \
  mv dist/calendar ${BASE_DIR}/custom_apps/ && \
  rm calendar.zip;

RUN curl -fsSL -o email-recovery.zip \
  "https://gitlab.e.foundation/e/infra/ecloud/nextcloud-apps/email-recovery/-/jobs/${EMAIL_RECOVERY_JOB_ID}/artifacts/download" && \
  unzip email-recovery.zip && \
  mv dist/email-recovery ${BASE_DIR}/custom_apps/ && \
  rm email-recovery.zip;


RUN curl -fsSL -o ecloud-theme-helper.zip \
  "https://gitlab.e.foundation/e/infra/ecloud/nextcloud-apps/ecloud-theme-helper/-/jobs/${THEME_HELPER_JOB_ID}/artifacts/download" && \
  unzip ecloud-theme-helper.zip && \
  mv dist/ecloud-theme-helper ${BASE_DIR}/custom_apps/ && \
  rm ecloud-theme-helper.zip;

RUN curl -fsSL -o murena_launcher.zip \
  "https://gitlab.e.foundation/e/infra/ecloud/nextcloud-apps/launcher/-/jobs/${LAUNCHER_JOB_ID}/artifacts/download" && \
  unzip murena_launcher.zip && \
  mv dist/murena_launcher ${BASE_DIR}/custom_apps/ && \
  rm murena_launcher.zip;

RUN curl -fsSL -o integration_google.tar.gz \
  "https://github.com/nextcloud-releases/integration_google/releases/download/v${GOOGLE_INTEGRATION_VERSION}/integration_google-v${GOOGLE_INTEGRATION_VERSION}.tar.gz" && \
  tar -xf integration_google.tar.gz -C ${BASE_DIR}/custom_apps/ && \
  chown -R www-data:www-data ${BASE_DIR}/custom_apps/integration_google && \
  rm integration_google.tar.gz;

RUN curl -fsSL -o ecloud-accounts.zip \
  "https://gitlab.e.foundation/e/infra/ecloud/nextcloud-apps/ecloud-accounts/-/jobs/${EA_JOB_ID}/artifacts/download" && \
  unzip ecloud-accounts.zip && \
  mv dist/ecloud-accounts ${BASE_DIR}/custom_apps/ && \
  rm ecloud-accounts.zip;

RUN curl -fsSL -o ecloud-dashboard.zip \
  "https://gitlab.e.foundation/e/infra/ecloud/nextcloud-apps/murena-dashboard/-/jobs/${DASHBOARD_JOB_ID}/artifacts/download" && \
  unzip ecloud-dashboard.zip && \
  mv dist/murena-dashboard ${BASE_DIR}/custom_apps/ && \
  rm ecloud-dashboard.zip;

RUN curl -fsSL -o snappymail.tar.gz \
  "https://snappymail.eu/repository/nextcloud/snappymail-${SNAPPY_VERSION}-nextcloud.tar.gz" && \
  tar -xf snappymail.tar.gz -C ${BASE_DIR}/custom_apps/ && \
  rm snappymail.tar.gz;

RUN curl -fsSL -o user_migration.zip \
  "https://gitlab.e.foundation/e/infra/ecloud/nextcloud-apps/user_migration/-/jobs/${USER_MIGRATION_JOB_ID}/artifacts/download" && \
  unzip user_migration.zip && \
  mv dist/user_migration ${BASE_DIR}/custom_apps/ && \
  rm user_migration.zip;

# Fix snappymail icon
RUN sed -i 's/logo-white-64x64.png/app.svg/' ${BASE_DIR}/custom_apps/snappymail/appinfo/info.xml

# Snappy theme
RUN curl -fsSL -o Murena-snappymail-theme.tar.gz \
  "https://gitlab.e.foundation/e/infra/ecloud/nextcloud-apps/snappymail-theme/-/archive/${SNAPPY_THEME_VERSION}/snappymail-theme-${SNAPPY_THEME_VERSION}.tar.gz" && \
  tar -xf Murena-snappymail-theme.tar.gz -C /tmp/ && \
  mkdir -p ${BASE_DIR}/themes/Murena && \
  mv /tmp/snappymail-theme-${SNAPPY_THEME_VERSION}/ ${BASE_DIR}/themes/Murena/snappymail && \
  chown -R www-data:www-data ${BASE_DIR}/themes/Murena/ && \
  rm -rf Murena-snappymail-theme.tar.gz

RUN curl -fsSL -o memories.tar.gz \
  "https://github.com/pulsejet/memories/releases/download/v${MEMORIES_VERSION}/memories.tar.gz" && \
  tar -xf memories.tar.gz -C ${BASE_DIR}/custom_apps/ && \
  rm memories.tar.gz;

RUN curl -fsSL -o drop_account.tar.gz \
  "https://packages.framasoft.org/projects/nextcloud-apps/drop-account/drop_account-${DROP_ACCOUNT_VERSION}.tar.gz" && \
  tar -xf drop_account.tar.gz -C ${BASE_DIR}/custom_apps/ && \
  rm drop_account.tar.gz;



# Remove unzip when unzipping is done
RUN apt-get -y remove unzip

# force eCloud theme not to be disabled even when there is an upgrade process launched
RUN sed -i "s/\$systemConfig->setValue('theme', '');/\$systemConfig->setValue('theme', 'eCloud');/g" ${BASE_DIR}/lib/base.php

# Disable editor feature of media viewer
RUN sed -i 's/canEdit(){var e,t;return!this.isMobile/canEdit(){var e,t;return!this.isMobile\&\&false/' ${BASE_DIR}/apps/viewer/js/viewer-main.js

ENTRYPOINT ["/custom_entrypoint.sh"]
CMD ["php-fpm"]

From nextcloud as selfhost
ARG BASE_DIR="/usr/src/nextcloud"
ARG TMP_PATCH_DIR="/tmp/build_patches"
ARG THEME_VERSION="26.0.2"
ARG USER_BACKEND_RAW_SQL_VERSION="2.0.1"
ARG SELFHOST_THEME_VERSION="2.0.0"
ARG IS_SELFHOST=true

# Patches
COPY patches/ ${TMP_PATCH_DIR}/
RUN cd ${BASE_DIR} && patch -p0 < ${TMP_PATCH_DIR}/002-login-without-domain.patch
RUN cd ${BASE_DIR} && patch -p0 < ${TMP_PATCH_DIR}/006-recovery-email-changes.patch
RUN patch -u ${BASE_DIR}/apps/settings/lib/Settings/Personal/ServerDevNotice.php -i ${TMP_PATCH_DIR}/007-remove-dev-notice.patch
RUN cd ${BASE_DIR} && patch -p0 < ${TMP_PATCH_DIR}/009-help-links.patch
RUN patch -u ${BASE_DIR}/lib/private/Updater.php -i ${TMP_PATCH_DIR}/010-disable-app-store-upgrade.patch
RUN cd ${BASE_DIR} && patch -p0 < ${TMP_PATCH_DIR}/011-privacy-settings.patch
RUN patch -u ${BASE_DIR}/lib/private/Authentication/Token/PublicKeyTokenProvider.php -i ${TMP_PATCH_DIR}/013-revert-token-password-update.patch
RUN cd ${BASE_DIR} && patch -p0 < ${TMP_PATCH_DIR}/014-add-mail-usage.patch
RUN cd ${BASE_DIR} && patch -p0 < ${TMP_PATCH_DIR}/015-email-mail-template.patch
RUN patch -u ${BASE_DIR}/core/Command/User/Setting.php -i ${TMP_PATCH_DIR}/018-occ-user-setting.patch
RUN patch -u ${BASE_DIR}/apps/files/js/files.js -i ${TMP_PATCH_DIR}/021-repeated-storage-dialog-fix.patch
RUN cd ${BASE_DIR} && patch -u ${BASE_DIR}/3rdparty/sabre/vobject/lib/ITip/Broker.php -i ${TMP_PATCH_DIR}/022-significantchange.patch
RUN cd ${BASE_DIR} && patch -p0 < ${TMP_PATCH_DIR}/026-primary-color-fix.patch
RUN patch -u ${BASE_DIR}/lib/private/Template/JSResourceLocator.php -i ${TMP_PATCH_DIR}/031-theme-custom-app-translations.patch
RUN patch -u ${BASE_DIR}/lib/private/L10N/Factory.php -i ${TMP_PATCH_DIR}/032-select-lang-from-session.patch
# UserConfigChangedEvent Ref: https://github.com/nextcloud/server/pull/42039
RUN cd ${BASE_DIR} && patch -p1 < ${TMP_PATCH_DIR}/036-user-config-change-event.patch

# fixed Mailer.php error which is fixed in upstream,so we need to remove this in Nextcloud 29 upgrade
RUN patch -u ${BASE_DIR}/lib/private/Mail/Mailer.php -i ${TMP_PATCH_DIR}/037-mailer-template-fix.patch
RUN rm -rf ${TMP_PATCH_DIR}

# Custom theme
RUN curl -fsSL -o eCloud-theme.tar.gz \
  "https://gitlab.e.foundation/e/infra/ecloud/nextcloud-apps/nextcloud-theme/-/archive/${THEME_VERSION}/nextcloud-theme-${THEME_VERSION}.tar.gz" && \
  tar -xf eCloud-theme.tar.gz -C /tmp/ && \
  mv /tmp/nextcloud-theme-${THEME_VERSION}/ ${BASE_DIR}/themes/eCloud && \
  chown -R www-data:www-data ${BASE_DIR}/themes/eCloud/ && \
  rm -rf eCloud-theme.tar.gz ${BASE_DIR}/themes/example/

# User Backend
RUN curl -fsSL -o user_backend_sql_raw.tar.gz \
  "https://github.com/PanCakeConnaisseur/user_backend_sql_raw/releases/download/v${USER_BACKEND_RAW_SQL_VERSION}/user_backend_sql_raw.tar.gz" && \
  tar -xf user_backend_sql_raw.tar.gz -C ${BASE_DIR}/custom_apps/ && \
  rm user_backend_sql_raw.tar.gz;

# Selfhost theme
RUN if [ "$IS_SELFHOST" = true ]; then \
  curl -fsSL -o selfhost-theme-helper.tar.gz \
  "https://gitlab.e.foundation/e/infra/ecloud/nextcloud-apps/selfhost-theme-helper/-/archive/${SELFHOST_THEME_VERSION}/selfhost-theme-helper-${SELFHOST_THEME_VERSION}.tar.gz" && \
  tar -xf selfhost-theme-helper.tar.gz -C /tmp/ && \
  mv /tmp/selfhost-theme-helper-${SELFHOST_THEME_VERSION}/ ${BASE_DIR}/custom_apps/selfhost-theme-helper && \
  chown -R www-data:www-data ${BASE_DIR}/custom_apps/selfhost-theme-helper/ && \
  rm -f selfhost-theme-helper.tar.gz;\
  fi




# Set default class of hidden to settings-hint
RUN sed -i 's/settings-hint/settings-hint hidden/' ${BASE_DIR}/apps/settings/templates/settings/personal/security/twofactor.php

RUN sed -i 's/ in Nextcloud/ /' ${BASE_DIR}/custom_apps/integration_google/js/integration_google-personalSettings.js
RUN sed -i 's/Nextcloud administrator/administrator/' ${BASE_DIR}/custom_apps/integration_google/js/integration_google-personalSettings.js


# Fix the navbar entries
RUN sed -i 's/this.appLimit=e/this.appLimit=this.appList.length/' ${BASE_DIR}/dist/core-main.js

# Fix total quota value for users in settings and files
RUN sed -i "s/'total'/'quota'/" ${BASE_DIR}/apps/settings/lib/Settings/Personal/PersonalInfo.php
RUN sed -i "s/\['total'\]/\['quota'\]/" ${BASE_DIR}/apps/provisioning_api/lib/Controller/AUserData.php

# Fix API call in files script
RUN sed -i "s/ajax\/getstoragestats/api\/v1\/stats/g" ${BASE_DIR}/apps/files/js/files.js

#fix for who can access data- its needed with 011-privacy-settings.patch
RUN sed -i 's|Your home storage is encrypted using {linkopen}server-side-encryption ↗{linkclose} with a master key\. It means that administrators can access your files, but not read their content\.|Like in most cloud services, a reduced number of administrators can see your files and all the information in the database as they need to make backups, perform upgrades, reset passwords, etc.<br>{linkopen1}Learn more about this topic here.  ↗{linkclose1}|g' ${BASE_DIR}/apps/privacy/js/privacy-main.js
RUN sed -i 's/t((0,n\.Iu)("privacy","Your files on external storages may be encrypted using {linkopen}server-side-encryption ↗{linkclose} with a master key based on their configuration\."))/t("{linkopen}"+(0,n\.Iu)("privacy","To protect your data, we have implemented server-side-encryption on our servers which hides by default the content of your files and notes even to these administrators")+"↗{linkclose}")/g' ${BASE_DIR}/apps/privacy/js/privacy-main.js
RUN sed -i 's|t=t=>e.push(t.replace("{linkopen}",'"'"'<a href="https://nextcloud.com/blog/encryption-in-nextcloud/" target="_blank" rel="noreferrer noopener" class="encryption__link">'"'"').replace("{linkclose}","</a>"))|t=t=>e.push(t.replace("{linkopen}",'"'"'<a href="https://nextcloud.com/blog/encryption-in-nextcloud/" target="_blank" rel="noreferrer noopener" class="encryption__link">'"'"').replace("{linkclose}","</a>"))|g'  ${BASE_DIR}/apps/privacy/js/privacy-main.js
RUN sed -i 's|.replace("{linkclose}","</a>")|.replace("{linkclose}","</a>").replace("{linkopen1}",'"'"'<a href="https://doc.e.foundation/support-topics/services#can-the-ecloud-admin-access-my-data" target="_blank" rel="noreferrer noopener" class="encryption__link">'"'"').replace("{linkclose1}","</a>")|g' ${BASE_DIR}/apps/privacy/js/privacy-main.js

From selfhost as ecloud
ARG BASE_DIR="/usr/src/nextcloud"
ARG TMP_PATCH_DIR="/tmp/build_patches"
ARG THEME_VERSION="28.0.2"
ARG LDAP_WRITE_SUPPORT_VERSION="1.10.0"
ARG IS_SELFHOST=false

RUN curl -fsSL -o ldap_write_support.tar.gz \
  "https://github.com/nextcloud-releases/ldap_write_support/releases/download/v${LDAP_WRITE_SUPPORT_VERSION}/ldap_write_support-v${LDAP_WRITE_SUPPORT_VERSION}.tar.gz" && \
  tar -xf ldap_write_support.tar.gz -C ${BASE_DIR}/custom_apps && \
  rm ldap_write_support.tar.gz

# Patches
COPY patches/ ${TMP_PATCH_DIR}/
RUN patch -u ${BASE_DIR}/core/templates/layout.user.php -i ${TMP_PATCH_DIR}/003-contact-search-removal.patch
RUN patch -u ${BASE_DIR}/core/Controller/ContactsMenuController.php -i ${TMP_PATCH_DIR}/004-contact-search-controller-removal.patch
RUN cd ${BASE_DIR} && patch -p0 < ${TMP_PATCH_DIR}/005-autocomplete-user-leak-core.patch
RUN patch -u ${BASE_DIR}/core/templates/layout.guest.php -i ${TMP_PATCH_DIR}/016-login-screen.patch
RUN patch -u ${BASE_DIR}/lib/private/Notification/Manager.php -i ${TMP_PATCH_DIR}/020-fairuse-notification-fix.patch
RUN cd ${BASE_DIR} && patch -u ${BASE_DIR}/apps/user_ldap/lib/User_LDAP.php -i ${TMP_PATCH_DIR}/023-ldap-check-pwd-optimization.patch
RUN patch -u ${BASE_DIR}/lib/private/User/Manager.php -i ${TMP_PATCH_DIR}/025-optimize-get-by-email.patch
RUN patch -u ${BASE_DIR}/apps/dav/lib/Connector/Sabre/Principal.php -i ${TMP_PATCH_DIR}/027-displayname-user-leak-dav.patch
RUN patch -u ${BASE_DIR}/apps/dav/lib/HookManager.php -i ${TMP_PATCH_DIR}/028-default-task-calendar.patch
RUN patch -u ${BASE_DIR}/apps/provisioning_api/lib/Controller/UsersController.php -i ${TMP_PATCH_DIR}/029-restrict-user-to-change-primary-email.patch
RUN patch -u ${BASE_DIR}/lib/private/Security/VerificationToken/VerificationToken.php -i ${TMP_PATCH_DIR}/033-verification-token-private.patch
RUN patch -u ${BASE_DIR}/lib/private/AppFramework/Middleware/Security/CORSMiddleware.php -i ${TMP_PATCH_DIR}/034-oidc-bearer-token-auth.patch
#fixed regression in generic event type for nextcloud 28.0.9
RUN patch -u ${BASE_DIR}/apps/dav/lib/AppInfo/Application.php -i  ${TMP_PATCH_DIR}/038-generic-event-fix.patch

#commenting code to re-enable account creation
RUN patch -u ${BASE_DIR}/lib/private/User/Session.php -i  ${TMP_PATCH_DIR}/039-reenable-ac-comment-setupfs.patch
RUN patch -u ${BASE_DIR}/lib/base.php -i  ${TMP_PATCH_DIR}/040-reenable-ac-comment-exception.patch
RUN patch -u ${BASE_DIR}/lib/private/Lockdown/LockdownManager.php -i  ${TMP_PATCH_DIR}/041-return-false-canfilesystem.patch

RUN rm -rf ${TMP_PATCH_DIR}


# Remove user avatar generation for system addressbook card
RUN sed -i 's/$this->getAvatarImage($user)/null/' ${BASE_DIR}/apps/dav/lib/CardDAV/Converter.php

# show reset pwd page with a query param
# RUN cd ${BASE_DIR}/dist && sed -i 's/resetPassword:!1/resetPassword:!1||Z.showResetPassword==="1"/' core-login.js

# add attr about how many notifications to notif icon
RUN sed -i 's/attrs:{id:"notifications",/attrs:{id:"notifications","data-has-notifications":0!==t.notifications.length,/' ${BASE_DIR}/apps/notifications/js/notifications-node_modules_moment_locale_sync_recursive_-src_NotificationsApp_vue-data_image_svg_xml_base64-ab95a3.js
# Add data-object-type to notification
RUN sed -i 's/"data-id":t.notificationId,/"data-id":t.notificationId,"data-object-type":t.objectType,/' ${BASE_DIR}/apps/notifications/js/notifications-node_modules_moment_locale_sync_recursive_-src_NotificationsApp_vue-data_image_svg_xml_base64-ab95a3.js

# Rename Memories to Gallery
RUN sed -i 's/<name>Memories<\/name>/<name>Gallery<\/name>/g' ${BASE_DIR}/custom_apps/memories/appinfo/info.xml
RUN cd ${BASE_DIR}/custom_apps/memories/js && sed -i 's/Memories has been/Gallery has been/g' memories-main.js && sed -i 's/Memories has been/Gallery has been/g' memories-admin.js
RUN cd ${BASE_DIR}/custom_apps/memories/js && sed -i 's/Memories Settings/Gallery Settings/g' memories-main.js

# autocomplete leak tweak apps frontend with sed, disable group suggestion

RUN cd ${BASE_DIR}/custom_apps/contacts && sed -i ' s/"GROUP","INDIVIDUAL"/"INDIVIDUAL"/g' js/contacts-main.js
RUN cd ${BASE_DIR}/custom_apps/calendar && sed -i 's/"GROUP","INDIVIDUAL"/"INDIVIDUAL"/g' js/calendar-main.js
RUN cd ${BASE_DIR}/custom_apps/calendar && sed -i 's/{name:\[a,"displayname"\]},//' js/calendar-main.js
RUN cd ${BASE_DIR}/custom_apps/notes && sed -i 's/<step>OCA\\Notes\\Migration\\EditorHint<\/step>//g' appinfo/info.xml
RUN cd ${BASE_DIR}/apps/theming && sed -i 's/<step>OCA\\Theming\\Migration\\MigrateUserConfig<\/step>//g' appinfo/info.xml

# sharing search by email
RUN cd ${BASE_DIR}/custom_apps/contacts && sed -i 's/kM.principalPropertySearchByDisplayname(t);/kM.principalPropertySearch(\[{name:\["http:\/\/sabredav.org\/ns","email-address"\]\}],t,"anyof");/g' js/contacts-main.js

# Remove colored background from email template logo
RUN sed -i 's/$this->header, \[$this->themingDefaults->getDefaultColorPrimary()/$this->header, \["none"/' ${BASE_DIR}/lib/private/Mail/EMailTemplate.php

# Remove changeAvatar in changeUserHook of ldap_write_support as it throws errors
RUN sed -i 's/$this->changeAvatar/\/\/ $this->changeAvatar/' ${BASE_DIR}/custom_apps/ldap_write_support/lib/LDAPUserManager.php


# Remove 'fix broken values of calendar objects' repair step
RUN sed -i '/<step>OCA\\DAV\\Migration\\CalDAVRemoveEmptyValue<\/step>/d' ${BASE_DIR}/apps/dav/appinfo/info.xml

# Hide `Play sound when a call started (requires Nextcloud Talk)` checkbox from notification setting
RUN sed -i 's/attrs:{checked:e\.config\.sound_talk}/staticClass:"notification_talk",attrs:{checked:e\.config\.sound_talk}/g' ${BASE_DIR}/apps/notifications/js/notifications-settings.js

# Hide `Navigation bar settings` from theming
RUN sed -i 's/{attrs:{name:t.t("theming","Navigation bar settings")}}/{staticClass:"navigation-bar-settings",attrs:{name:t.t("theming","Navigation bar settings")}}/g' ${BASE_DIR}/dist/theming-personal-theming.js


# Custom theme
RUN rm -rf ${BASE_DIR}/themes/eCloud && \
  curl -fsSL -o eCloud-theme.tar.gz \
  "https://gitlab.e.foundation/e/infra/ecloud/nextcloud-apps/nextcloud-theme/-/archive/${THEME_VERSION}/nextcloud-theme-${THEME_VERSION}.tar.gz" && \
  tar -xf eCloud-theme.tar.gz -C /tmp/ && \
  mv /tmp/nextcloud-theme-${THEME_VERSION}/ ${BASE_DIR}/themes/eCloud && \
  chown -R www-data:www-data ${BASE_DIR}/themes/eCloud/ && \
  rm -rf eCloud-theme.tar.gz ${BASE_DIR}/themes/example/

# Remove Selfhost theme
RUN if [ "$IS_SELFHOST" = false ]; then \
  rm -rf ${BASE_DIR}/custom_apps/selfhost-theme-helper;\
  fi

# Seds to fix displayname save regression
RUN sed -i "s/'email' ||/'email'/" ${BASE_DIR}/apps/settings/js/federationsettingsview.js
RUN sed -i "s/field === 'displayname'//" ${BASE_DIR}/apps/settings/js/federationsettingsview.js

# Seds to allow deletion of LDAP users
RUN sed -i "s/return in_array(\$user->getBackendClassName(), \['Database', 'Guests'\]);/return in_array(\$user->getBackendClassName(), ['Database', 'Guests', 'LDAP']);/" ${BASE_DIR}/custom_apps/drop_account/lib/Service/DeletionCapabilityService.php

# Seds to remove share option from notes
RUN sed -i ':a;N;$!ba;s/t("NcActionButton",{on:{click:e.onToggleSharing},scopedSlots:e._u(\[{key:"icon",fn:function(){return\[t("ShareVariantIcon",{attrs:{size:20}})\]},proxy:!0}\])},\[e._v("\\n\\t\\t\\t"+e._s(e.t("notes","Share"))+"\\n\\t\\t")\]),e._v(" "),/''/g' ${BASE_DIR}/custom_apps/notes/js/notes-main.js

# Add id to delete account settings section and hide it by default
Run sed -i 's/"NcSettingsSection",{attrs:{/"NcSettingsSection",{attrs:{id:"delete-account-settings-section",style:"visibility:hidden;",/' ${BASE_DIR}/custom_apps/drop_account/js/drop_account-personal-settings.js

# Fix of https://github.com/nextcloud/server/commit/e727a3b00bd9b574279d25c006a0bc6b8fa4eec3 for availability setting
RUN sed -i 's/n.data/await n.text()/' ${BASE_DIR}/dist/dav-settings-personal-availability.js